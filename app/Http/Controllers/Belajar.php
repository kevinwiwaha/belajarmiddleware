<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class Belajar extends Controller
{

    public function index()
    {
        dump(Auth::user()->role_id);
    }
    public function route1()
    {
        if (Auth::user()->role_id == 2) {
            echo 'ini adalah route 1';
            die;
        }
        abort(403);
    }
    public function route2()
    {
        if (Auth::user()->role_id >= 1) {
            echo 'ini adalah route 2';
            die;
        }
        abort(403);
    }
    public function route3()
    {
        if (Auth::user()->role_id >= 0) {
            echo 'ini adalah route 3';
            die;
        }
        abort(403);
    }
}
